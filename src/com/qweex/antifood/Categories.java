package com.qweex.antifood;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class Categories extends ListActivity implements AdapterView.OnItemLongClickListener {

    DbAbstraction.Table categories;
    Dialog addDialog;
    EditText addName;
    LongClickMenu longClickMenu;
    DbAbstraction.TableRow editingRow;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        categories = MyActivity.categories;

        addName = new EditText(this);
        addName.setHint("Name");

        addDialog = new AlertDialog.Builder(this)
                .setTitle("Add Category")
                .setView(addName)
                .setPositiveButton("Save", addCategory)
                .setNegativeButton("Cancel", null).create();

        getListView().setOnItemLongClickListener(this);
        longClickMenu = new LongClickMenu(this);

        updateAdapter();
    }

    DialogInterface.OnClickListener addCategory = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String name = addName.getText().toString();
            if(name.length()==0) {
                Toast.makeText(Categories.this, "Name cannot be empty!", Toast.LENGTH_SHORT).show();
                return;
            }
            else if(editingRow!=null)
                editingRow.updateVal(new DbAbstraction.FieldValue("name", name)).updateCommit();
            else if(categories.getRows(new DbAbstraction.FieldValue("name", name)).size()>0) {
                Toast.makeText(Categories.this, "Name is already taken!", Toast.LENGTH_SHORT).show();
                return;
            }
            else
                categories.addRow(new DbAbstraction.FieldValueArrayBuilder().add("name", name).build());
            updateAdapter();
            addDialog.dismiss();
        }
    };

    public void updateAdapter() {
        setListAdapter(MyActivity.db.new DbAbstractionAdapter(
                this,
                android.R.layout.simple_list_item_1,
                categories.getAllRows(),
                new String[]{"name"},
                new int[]{android.R.id.text1}
        ));
    }

    final int ADD_ID = 0;
    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        m.add(0, ADD_ID, 0, "Add").getItemId();
        return super.onCreateOptionsMenu(m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem m) {
        switch(m.getItemId()) {
            case ADD_ID:
                addDialog.show();
        }
        return super.onOptionsItemSelected(m);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        //String name = ((TextView)view).getText().toString();

        String name = ((TextView)view.findViewById(android.R.id.text1)).getText().toString();
        longClickMenu.show(name);

        return false;
    }

    // Dialog to show when a user long presses a Category.
    class LongClickMenu implements AdapterView.OnItemClickListener{
        Dialog d;
        DbAbstraction.TableRow row;

        public void show(final String name) {
            row = categories.getRow(new DbAbstraction.FieldValue("name", name));
            d.show();
        }

        public LongClickMenu(Context that) {
            AlertDialog.Builder builder = new AlertDialog.Builder(that);

            ListView list = new ListView(that);
            String[] stringArray = new String[] { getResources().getString(R.string.edit),
                    getResources().getString(R.string.delete) };
            ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(that, android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
            list.setAdapter(modeAdapter);
            list.setOnItemClickListener(this);
            builder.setView(list);

            d = builder.create();
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            d.dismiss();
            switch(i) {
                case 0: //edit
                    editingRow = row;
                    addName.setText(row.getVal("name"));
                    addDialog.show();
                    break;
                case 1: //delete
                    new AlertDialog.Builder(Categories.this)
                            .setTitle(row.getVal("name"))
                            .setMessage(R.string.confirm_delete)
                            .setNegativeButton(R.string.no, null)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    row.remove();
                                    updateAdapter();
                                }
                            })
                            .show();
                    break;
            }
        }
    }
}
