package com.qweex.antifood;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.*;


public class DbAbstraction extends SQLiteOpenHelper {

    enum FIELD_TYPE {TEXT, INTEGER}
    private SQLiteDatabase database;

    public DbAbstraction(Context context, String name) {
        super(context, name, null, 1);
        database = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}



    ////////////////////////////////////////////////////////////////////////////////


    public class TableBuilder {
        ArrayList<Field> fields = new ArrayList<Field>();
        String tableName;
        HashSet<Table> belongsTo = new HashSet<Table>();
        HashSet<Table> hasMany = new HashSet<Table>();
        public TableBuilder(String tableName) {
            this.tableName = tableName;
        }

        public TableBuilder addField(String name, FIELD_TYPE type) {
            fields.add(new Field(name, type));
            return this;
        }

        public TableBuilder belongsTo(Table f) throws IllegalArgumentException {
            if(!belongsTo.contains(f)) {
                fields.add(new Field(f.getName() + "_id", FIELD_TYPE.INTEGER));
                belongsTo.add(f);
            }
            return this;
        }
        public TableBuilder hasMany(Table f) throws IllegalArgumentException {
            hasMany.add(f);
            return this;
        }

        public Table make() {
            Table t = new Table(tableName, fields);
            for(Table b : belongsTo)
                t.belongsTo(b);
            for(Table h : hasMany)
                t.hasMany(h);
            return t;
        }

        public DbAbstraction getDb() {
            return DbAbstraction.this;
        }
    }

    public class Table {

        HashSet<Field> fields;
        String tableName;

        HashMap<String,Table> belongsTo = new HashMap<String,Table>();
        HashMap<String,Table> hasMany = new HashMap<String,Table>();

        private Table(String tableName, ArrayList<Field> fields) {
            this.fields = new HashSet<Field>(fields);
            this.tableName = tableName;

            String[] query = new String[fields.size()+1];
            query[0] = "CREATE TABLE IF NOT EXISTS " + tableName+ " (_id integer primary key autoincrement";

            for(int i=0; i<fields.size(); i++) {
                Field field = fields.get(i);
                query[i+1] = field.name + " " + field.type;
            }
            database.execSQL(TextUtils.join(", ", query) + ");");
            adjustColumns();
        }

        private void adjustColumns() {
            //TODO: Handle things like changing column types?
            Cursor cursor = database.rawQuery("SELECT * FROM " + tableName + " LIMIT 1", null);
            HashSet<String> inSQL = new HashSet<String>(Arrays.asList(cursor.getColumnNames()));
            HashSet<String> inObject = new HashSet<String>(Arrays.asList(getFieldNames()));

            Set<String> removedFields = new HashSet<String>(inSQL);
            removedFields.removeAll(inObject);
            Set<String> addedFields = new HashSet<String>(inObject);
            addedFields.removeAll(inSQL);

            for(String removed : removedFields)
                database.execSQL("ALTER TABLE " + tableName + " DROP COLUMN " + removed + ";");
            for(String added : addedFields) {
                for(Field f : fields)
                    if(f.name.equals(added))
                        database.execSQL("ALTER TABLE " + tableName + " ADD COLUMN " + f.name + " " + f.type + ";");
            }
        }

        // States that otherTable has a column named 'tableName_id'
        private void hasMany(Table otherTable) {
            if(hasMany.containsKey(otherTable.getName()))
                return;
            hasMany.put(otherTable.getName(), otherTable);
            otherTable.belongsTo(this);
        }

        // States that THIS table has a column named 'otherTable.tableName_id'
        private void belongsTo(Table otherTable) {
            if(belongsTo.containsKey(otherTable.getName()))
                return;
            belongsTo.put(otherTable.getName(), otherTable);
            otherTable.hasMany(this);
        }

        public String getName() { return tableName;}



        public ArrayList<TableRow> getRows(FieldValue query) throws IllegalArgumentException { return getRows(query, null, false); }
        public ArrayList<TableRow> getRows(FieldValue query, Order orderBy) throws IllegalArgumentException { return getRows(query, orderBy, false); }
        public ArrayList<TableRow> getRows(FieldValue query, boolean like) throws IllegalArgumentException { return getRows(query, null, like); }
        public ArrayList<TableRow> getRows(FieldValue query, Order orderBy, boolean like) throws IllegalArgumentException {

            //TODO: Ensure query.field.name is in field names

            return cursorToRows(database.query(tableName,       /* table */
                    getFieldNames(),                            /* columns */
                    query.field +
                            (like ? " like %?%" : " == ?"),     /* where or selection */
                    new String[] {query.value},                 /* selectionArgs */
                    null,                                       /* groupBy */
                    null,                                       /* having */
                    orderBy!=null ? orderBy.toString() : null)  /* orderBy */
            );
        }

        public ArrayList<TableRow> getAllRows() throws IllegalArgumentException {
            Cursor c = database.query(tableName, getFieldNames(), null, null, null, null, null);
            return cursorToRows(c);
        }

        private ArrayList<TableRow> cursorToRows(Cursor c) {
            ArrayList<TableRow> x = new ArrayList<TableRow>();
            if(c.getCount()==0)
                return x;

            c.moveToFirst();
            while(!c.isAfterLast()) {
                x.add(new TableRow(this, c));
                c.moveToNext();
            }
            return x;
        }

        public TableRow getRow(FieldValue query) throws IllegalArgumentException { return getRow(query, false); }
        public TableRow getRow(FieldValue query, boolean like) throws IllegalArgumentException {

            //TODO: Ensure query.field.name is in field names

            return new TableRow(this,
                    database.query(tableName,                           /* table */
                            getFieldNames(),                            /* columns */
                            query.field +
                                    (like ? " like %?%" : " == ?"),    /* where or selection */
                            new String[] {query.value},                 /* selectionArgs */
                            null,                                       /* groupBy */
                            null,                                       /* having */
                            null)                                       /* orderBy */
            );
        }

        public TableRow addRow(FieldValue[] row) {
            ContentValues newRow = new ContentValues();

            String[] myFields = getFieldNames();
            for(FieldValue fv : row) {
                newRow.put(fv.field, fv.value);
                boolean invalid = true;
                for(String myField : myFields)
                    if(myField.equals(fv.field)) {
                        invalid = false;
                        break;
                    }
                if(invalid)
                    throw new IllegalArgumentException();
            }
            return new TableRow(this, row, database.insert(tableName, null, newRow));
        }

        public TableRow updateRow(DbAbstraction.TableRow row) {
            ContentValues updatedRow = new ContentValues();

            for(Field f : fields) {
                updatedRow.put(f.name, row.getVal(f.name));
            }
            database.update(tableName, updatedRow, "_id = ?", new String[] { Long.toString(row.id())});
            return row;
        }

        private void removeRow(TableRow row) {
            database.delete(tableName, "_id = ?", new String[] {row.getVal("_id")});
        }

        public void removeAllRows() {
            for(TableRow row : getAllRows())
                removeRow(row);
        }

        public HashSet<Field> getFields() { return fields; }

        private String[] getFieldNames() {
            ArrayList<String> x = new ArrayList<String>();
            x.add("_id");
            for(Field f : fields) {
                x.add(f.name);
            }
            return x.toArray(new String[x.size()]);
        }

        public DbAbstraction getDb() {
            return DbAbstraction.this;
        }
    }

    public class TableRow {
        Table ofTable;
        HashMap<String, String> myValues = new HashMap<String, String>();
        long id;

        private TableRow(Table of, Cursor c) {
            if(c.getCount()==0)
                return;
            if(c.isBeforeFirst())
                c.moveToFirst();
            ofTable = of;
            id = c.getLong(c.getColumnIndex("_id"));
            for(String f : ofTable.getFieldNames()) {
                myValues.put(f, c.getString(c.getColumnIndex(f)));
            }
        }

        private TableRow(Table of, FieldValue[] val, long id) {
            this.id = id;
            for(FieldValue v : val) {
                myValues.put(v.field, v.value);
            }
        }

        public String getVal(String fieldName) throws IllegalArgumentException{
            if(!myValues.containsKey(fieldName))
                throw new IllegalArgumentException();
            return myValues.get(fieldName);
        }


        public TableRow getParent(Table otherTable) throws IllegalArgumentException {
            if(!ofTable.belongsTo.containsKey(otherTable.getName()))
                throw new IllegalArgumentException();
            return otherTable.getRow(new FieldValue("_id", getVal(otherTable.getName() + "_id")));
        }

        public ArrayList<TableRow> getChildren(Table otherTable) throws IllegalArgumentException{
            if(!ofTable.belongsTo.containsKey(otherTable.getName()))
                throw new IllegalArgumentException();
            return otherTable.getRows(new FieldValue("_id", getVal(otherTable.getName() + "_id")));
        }

        public HashSet<Field> getFields() { return ofTable.getFields(); }

        public void remove() {
            ofTable.removeRow(this);
            ofTable = null;
            myValues = null;
        }

        public TableRow updateVal(FieldValue fv) {
            myValues.put(fv.field, fv.value);
            return this;
        }

        public TableRow updateCommit() {
            return ofTable.updateRow(this);
        }

        public long id() {
            return id;
        }
    }

    public static class Field {
        public String name;
        public FIELD_TYPE type;
        public Field(String n, FIELD_TYPE t) { name=n; type=t; }
    }

    public static class FieldValue {
        public String field;
        public String value;
        public FieldValue(String f, String v) { field=f; value=v; }

    }

    public static class FieldValueArrayBuilder {
        ArrayList<FieldValue> data = new ArrayList<FieldValue>();
        public FieldValueArrayBuilder() {}

        public FieldValueArrayBuilder add(String field, String value) {
            data.add(new FieldValue(field, value));
            return this;
        }
        public FieldValueArrayBuilder belongsTo(Table table, TableRow row) {
            data.add(new FieldValue(table.getName() + "_id", row.id() + ""));
            return this;
        }

        public FieldValue[] build() {
            return data.toArray(new FieldValue[data.size()]);
        }
    }

    public static class Order {
        String name;
        boolean desc;
        public Order(String n, boolean d) { name = n; desc = d; }
        public Order(String n) { name = n; desc = true; }

        @Override
        public String toString() {
            return name + (desc ? " DESC" : " ASC");
        }
    }


    public class DbAbstractionAdapter extends ArrayAdapter<TableRow> {
        ArrayList<TableRow> data;
        int resourceID, dropdownResourceID;
        String[] from;
        int[] to;
        LayoutInflater inflater;

        public DbAbstractionAdapter(Context context, int resource, TableRow[] objects, String[] from, int[] to) {
            this(context, resource, Arrays.asList(objects), from, to);
        }

        @Override
        public void setDropDownViewResource(int r) {
            dropdownResourceID = r;
        }

        public DbAbstractionAdapter(Context context, int resource, List<TableRow> objects, String[] from, int[] to) {
            super(context, resource, objects);
            resourceID = resource;
            data = new ArrayList<TableRow>(objects);
            this.from = new String[from.length];
            System.arraycopy(from, 0, this.from, 0, from.length);
            this.to = new int[to.length];
            System.arraycopy(to, 0, this.to, 0, to.length);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getDatView(position, convertView, parent, resourceID);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getDatView(position, convertView, parent, dropdownResourceID);
        }

        View getDatView(int position, View convertView, ViewGroup parent, int resID) {
            if(convertView == null)
                convertView = inflater.inflate(resID, null);

            TableRow row = getItem(position);
            for(int i=0; i<from.length && i<to.length; i++) {
                TextView toView = ((TextView)convertView.findViewById(to[i]));
                toView.setText(row.getVal(from[i]));
            }

            return convertView;
        }

    }
}
