package com.qweex.antifood;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.qweex.antifood.DbAbstraction.*;

public class MyActivity extends Activity
{

    static int UPDATED = 3481512;
    static Table recipes, ingredients, categories, join;
    static DbAbstraction db;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initDatabase();
    }

    public void initDatabase() {
        db = new DbAbstraction(this, "ANTIFOOD");

        //Recipe
        recipes = db.new TableBuilder("recipes")
                .addField("name", FIELD_TYPE.TEXT)
                .addField("prep", FIELD_TYPE.TEXT)
                .make();

        //Category
        categories = db.new TableBuilder("categories")
                .addField("name", FIELD_TYPE.TEXT)
                .make();

        //Item
        ingredients = db.new TableBuilder("ingredients")
                .addField("name", FIELD_TYPE.TEXT)
                .belongsTo(categories)
                .make();

        //RecipeItem
        join = db.new TableBuilder("recipe_ingredients")
                .belongsTo(recipes)
                .belongsTo(ingredients)
                .make();
    }

    final int CATEGORIES_ID = 0, ADD_INGREDIENT_ID = 1;
    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        m.add(0, CATEGORIES_ID, 0, "Categories");
        m.add(0, ADD_INGREDIENT_ID, 0, "Add Ingredient");
        return super.onCreateOptionsMenu(m);
    }

    final int A_INGREDIENT_HASH = AddIngredient.class.hashCode() % 0xffffff;
    @Override
    public boolean onOptionsItemSelected(MenuItem m) {
        switch(m.getItemId()) {
            case CATEGORIES_ID:
                startActivity(new Intent(this, Categories.class));
                break;
            case ADD_INGREDIENT_ID:
                startActivityForResult(new Intent(this, Categories.class), A_INGREDIENT_HASH);
                startActivity(new Intent(this, AddIngredient.class));
                break;
        }
        return super.onOptionsItemSelected(m);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == A_INGREDIENT_HASH && resultCode == UPDATED) {
            ListView lv = ((ListView)findViewById(R.id.ingredients_list));
            ((DbAbstractionAdapter)lv.getAdapter()).notifyDataSetChanged();
        }
    }
}
